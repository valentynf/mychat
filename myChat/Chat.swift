//
//  ChatViewListItemTableViewCell.swift
//  myChat
//
//  Created by Valentyn Filippov on 9/16/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import Firebase

struct Chat {
    
    let ref: DatabaseReference?
    let key: String
    let name: String
    
    init(name: String, key: String = "") {
        self.ref = nil
        self.key = key
        self.name = name
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let name = value["name"] as? String else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.name = name
        print(key)
    }
    
    func toAnyObject() -> Any {
        return [
            "name": name
        ]
    }
}

//
//  ViewController.swift
//  myChat
//
//  Created by Valentyn Filippov on 9/12/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import UIKit
import FirebaseAuth


class LoginScreenViewController: UIViewController {

    
    @IBOutlet var nameDisplayTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    
    @IBAction func logInButtonIsPressed(_ sender: Any) {
        signIn()
    }
    
    private func signIn() {
        guard let name = nameDisplayTextField.text, !name.isEmpty else {
            showMissingNameAlert()
            return
        }
        
        nameDisplayTextField.resignFirstResponder()
        
//        AppSettings.displayName = name // please check info about this (AppSetings in Delegate)
        
        Auth.auth().signInAnonymously(completion: nil)
    }
    
    private func showMissingNameAlert() {
        let ac = UIAlertController(title: "Display Name Required", message: "Please enter a display name.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "Okay", style: .default, handler: { _ in
            DispatchQueue.main.async {
                self.nameDisplayTextField.becomeFirstResponder()
            }
        }))
        present(ac, animated: true, completion: nil)
    }
    
}


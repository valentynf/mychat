//
//  MessageCollectionViewCell.swift
//  myChat
//
//  Created by Valentyn Filippov on 9/16/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import Firebase

struct Message{
    
    let ref: DatabaseReference?
    let key: String
    let text: String?
    let sentByUser: String
    
    init(text: String, sentByUser: String, key: String = "") {
        self.ref = nil
        self.text = text
        self.sentByUser = sentByUser
        self.key = key
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let text = value["text"] as? String,
            let sentByUser = value["sentByUser"] as? String else {
                return nil
        }
    
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.text = text
        self.sentByUser = sentByUser
    }
    
}

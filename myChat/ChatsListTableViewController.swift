//
//  ChatsListTableVCTableViewController.swift
//  myChat
//
//  Created by Valentyn Filippov on 9/16/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import UIKit
import Firebase

class ChatsListTableViewController: UITableViewController {
    
    var chats: [Chat] = []
    var user: User!
    let ref = Database.database().reference(withPath: "chats-list")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // other code here
        
        ref.observe(.value, with: { snapshot in
            print(snapshot.value as Any)
            
            var newChats: [Chat] = []
            
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let chatListItem = Chat(snapshot: snapshot) {
                    newChats.append(chatListItem)
                }
            }
            self.chats = newChats
            self.tableView.reloadData()
        })
    }
    
    // MARK: - Table view data source
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatItemCell", for: indexPath)
        
        // Configure the cell...
        let chatListItem = chats[indexPath.row]
        cell.textLabel?.text = chatListItem.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let chatsListItem = chats[indexPath.row]
            chatsListItem.ref?.removeValue()
        }
    }
    
    
    // MARK: - Navigation
    
    @IBAction func addChatButtonIsClicked(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "New Chat",
                                      message: "Enter the Name",
                                      preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            guard let textField = alert.textFields?.first,
                let text = textField.text else { return }
            
            let chatListItem = Chat(name: text)
            let chatListItemItemRef = self.ref.child(text.lowercased())
            chatListItemItemRef.setValue(chatListItem.toAnyObject())
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let chatWindowCollectionViewController = segue.destination as? ChatWindowCollectionViewController {
//            
//        }
    }
    
}

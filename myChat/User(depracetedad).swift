//
//  User.swift
//  myChat
//
//  Created by Valentyn Filippov on 9/16/18.
//  Copyright © 2018 Valentyn Filippov. All rights reserved.
//

import Firebase

struct Userd {
    let uid: String
    let email: String
    
    init(authData: Firebase.User) {
        uid = authData.uid
        email = authData.email!
    }
    
    init(uid: String, email: String) {
        self.uid = uid
        self.email = email
    }
}
